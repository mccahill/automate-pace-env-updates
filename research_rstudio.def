Bootstrap: docker
IncludeCmd: "yes"
From: rocker/rstudio

%runscript
	conda list | tail -n+3 | awk '{print $1, $2, "Anaconda"}' > /tmp/temp.info
	R --version | head -1 | awk '{print $1, $3, "CRAN"}' >> /tmp/temp.info
	R -e 'installed.packages(fields = c("Package", "Version"))[,c("Package", "Version")]' | tail -n+22 | grep -v '^>' | awk '{print $1, $3, "R_package"}' | perl -pe 's/"//g' >> /tmp/temp.info
	column -t /tmp/temp.info | sort -u --ignore-case

%post
	locale-gen "en_US.UTF-8"
	mkdir /Software
	mkdir /Work
	apt-get update
	apt-get install -y build-essential cmake curl ed git libsm6 libxrender1 libfontconfig1 lsb-release nettle-dev python-setuptools ruby software-properties-common vim wget zlib1g-dev apt-transport-https libgtk-3-0 bzip2 gfortran gettext libcairo2-dev libgit2-dev bsdmainutils
	apt-get update
	apt-get clean
	cd /Software
	wget http://repo.continuum.io/archive/Anaconda3-4.1.1-Linux-x86_64.sh
	bash Anaconda3-4.1.1-Linux-x86_64.sh -b -p /Software/anaconda3
	rm Anaconda3-4.1.1-Linux-x86_64.sh
	export PATH="/Software/anaconda3/bin:$PATH"
	conda update -y conda
	conda update -y anaconda
	conda install -y scikit-learn
	conda install -y theano
	conda install -y tensorflow
	conda install -y theano
	conda install -y caffe
	conda install -y jupyter
	mkdir -p /run/user
	R -e "install.packages('devtools',repos='http://archive.linux.duke.edu/cran/')"
	R -e "devtools::install_github('rstudio/tensorflow')"
	sed -i 's|PATH=$PATH:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin|PATH="/Software/anaconda3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"|' /environment
